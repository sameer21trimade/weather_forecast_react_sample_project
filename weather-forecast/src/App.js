import React from 'react';
import './App.css';
import Axios from 'axios'
import DisplayWeatherInfo from './weatherAppComponents/DisplayWeatherInfo'

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      location: { longitude: 0, latitude: 0 },
      weatherUpdate: {},
      locationGivenByUser: {}
    }
    this.onChangeEventHandler=this.onChangeEventHandler.bind(this)
    this.onSubmitEventHandler=this.onSubmitEventHandler.bind(this)
  }
  componentDidMount() {
    let deviceCurrentLocation = (position) => {
      let longitudeLocation = position.coords.longitude
      let latitudeLocation = position.coords.latitude
      let currentLocation = { longitude: longitudeLocation, latitude: latitudeLocation }
      this.setState({ location: currentLocation })      
      Axios.get(`http://api.weatherstack.com/current?access_key=ee2c00a09ba65e4467143d28625d3fa2&query=${this.state.location.latitude},${this.state.location.longitude}`)
        .then((apiData) => {
          let weatherInfo = {
            temperature: apiData.data.current.temperature,
            description: apiData.data.current.weather_descriptions,
            location: apiData.data.location.name,
            region: apiData.data.location.region,
            country: apiData.data.location.country,
            timeZone: apiData.data.location.timezone_id,            
            precipitation: apiData.data.current.precip,
            humidity: apiData.data.current.humidity,
            climateImage: apiData.data.current.weather_icons,
            wind_speed: apiData.data.current.wind_speed,
            pressure: apiData.data.current.pressure
          }
          this.setState({ weatherUpdate: weatherInfo })
        })
    }
    navigator.geolocation.getCurrentPosition(deviceCurrentLocation)
  }
  onChangeEventHandler(event) {
    this.setState({locationGivenByUser:event.target.value})    
  }
  onSubmitEventHandler(event) {
    event.preventDefault()    
    Axios.get(`http://api.weatherstack.com/current?access_key=ee2c00a09ba65e4467143d28625d3fa2&query=${this.state.locationGivenByUser}`)
        .then((apiData) => {
          let weatherInfo = {
            temperature: apiData.data.current.temperature,
            description: apiData.data.current.weather_descriptions,
            location: apiData.data.location.name,
            region: apiData.data.location.region,
            country: apiData.data.location.country,
            timeZone: apiData.data.location.timezone_id,
            precipitation: apiData.data.current.precip,
            humidity: apiData.data.current.humidity,
            climateImage: apiData.data.current.weather_icons,
            wind_speed: apiData.data.current.wind_speed,
            pressure: apiData.data.current.pressure
          }
          this.setState({ weatherUpdate: weatherInfo })
        })
  }
  render() {
    return (
      <DisplayWeatherInfo apiData={this.state.weatherUpdate} onSubmitEventHandler={this.onSubmitEventHandler} onChangeEventHandler={this.onChangeEventHandler} />
    )
  }
}

export default App
