import React from 'react'

function DisplayWeatherInfo(props) {
    let date = new Date();
    let todaysDate = date.getDate();
    // eslint-disable-next-line
    let month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var thisMonth = month[date.getMonth()];
    // eslint-disable-next-line
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    let day = weekday[date.getDay()];


    return (
        <div className="parentContainer">
            <div className="navigationBar">
                <h1>Weather Forecast</h1>
            </div>
            <form className="region" onSubmit={(event) => { props.onSubmitEventHandler(event) }}>
                <input className="enterRegion" placeholder="Enter Region..." type="text" onChange={(event) => { props.onChangeEventHandler(event) }} />
            </form>
            <h1 id="day">{day}, {todaysDate} {thisMonth}</h1>

            <div className="weatherInfo">

                <div className="displayTemperatureAndImg">

                    <h1 id="temperature">{props.apiData.temperature}<sup>o</sup>C </h1>

                    <img className="climateImg" src={props.apiData.climateImage} alt="climatePicture" />

                    <h2 className="weatherStatus">{props.apiData.description}</h2>

                </div>

                <div className="weatherForecastPropertiesContainer">

                    <h2 className="weatherForecastProperties">Location: {props.apiData.location}</h2>

                    <h2 className="weatherForecastProperties">State: {props.apiData.region}</h2>

                    <h2 className="weatherForecastProperties">Country: {props.apiData.country}</h2>

                    <h2 className="weatherForecastProperties">Time Zone: {props.apiData.timeZone}</h2>

                    <h2 className="weatherForecastProperties">Wind Speed: {props.apiData.wind_speed} km/hr</h2>

                    <h2 className="weatherForecastProperties">Pressure: {props.apiData.pressure} millibar</h2>

                    <h2 className="weatherForecastProperties">Precipitation: {props.apiData.precipitation} mm</h2>

                    <h2 className="weatherForecastProperties">Humidity: {props.apiData.humidity} %</h2>

                </div>
            </div>
        </div>
    )
}

export default DisplayWeatherInfo